# How to generate PSD2 X.509  certificate with custom Root sign  on OpenSSL

I will explain how to generate a certificate according Open Banking Public Key Infrastructure (PKI) according ETSI TS 119 495 V1.2.1 (2018-11) and  signed it by a root certificate as Certificate Authority (CA).
For the purpose of this tutorial, we won’t submit our Certificate Signing Request (CSR) to a real CA. Instead, we will play both roles: the certificate authority and the certificate applicant.

***Before we start generating a certificate it is very important that you execute the commands one after the other.***

So here's what we're gonna do
## Steps

 1.  Install OpenSSL.
 2.  Create folder and download configuration files for certificate generation.
 3.  Customize OpenSSL Configuration
 4.  Generate and verify Root key and certificate.
 5.  Generate and verify PSD2 QWAC X.509  certificate.
 6. Generate and verify PSD2 QSEAL X.509 key and  certificate.
 7.  Create OCSP Certificate
 8.  Start OCSP server
 9.  Verify certificate revocation
 10.  Revoke a certificate
 11. Publish your OCSP server to the internet using NGROCK
 


##  Install OpenSSL version
In order to do all of these things, we need to have OpenSSL installed
Download and install OpenSSL V1.1.1h from :
https://slproweb.com/products/Win32OpenSSL.html

After the installation press Windows+R to open “Run” box. Type “cmd” and then click  Ctrl+Shift+Enter to open an administrator Command Prompt.

To check which OpenSSL version is installed on windows machin, run command :

    openssl version
    
In order to avoid problems during the certificate generation OpenSSl version should to be v 1.1.1 h . When we are sure that we have the correct version of OpenSSl. We are ready to create folders and download configuration files for Generating of root certificate.



## Create folder and download configuration files for certificate generation.

start creating the folder in Command Prompt to run  following command :

    mkdir openbanking_CA
  >or create directory in Windows Explorer
  
Change the directory (folder) in Command Prompt

    cd openbanking_CA
>if you create folder in Windows explorer you should change same directory in Command Prompt

Create folder for private keys 

    mkdir private

Create folder for configuration files

	mkdir conf
    
Create folder for new certificate

	mkdir newcerts

download configuration files in  openbanking_ca/conf directory running following commands

download RootConfig.cnf

    curl https://bitbucket.org/obgeo/development-ca/src/master/conf/RootConfig.cnf -o conf/RootConfig.cnf

download PSD2_QSEAL_config.cnf

    curl https://bitbucket.org/obgeo/development-ca/src/master/conf/PSD2_QSEAL_config.cnf -o conf/PSD2_QSEAL_config.cnf
    
download PSD2_QWAC_config.cnf

    curl https://bitbucket.org/obgeo/development-ca/src/master/conf/PSD2_QWAC_config.cnf -o conf/PSD2_QWAC_config.cnf
     
>  or download it by web browser 
RootConfig.cnf
PSD2_QSEAL_config.cnf
PSD2_QWAC_config

I will explain in "Customize OpennSSL Configuration" section how to customize OpenSSL configuration files. According to the requirements of your TTP or ASPSP.

## Customize OpennSSL Configuration

The PSD2_QSEAL_config.cnf and PSD2_QWAC_config.conf configuration file is used to generate a CSR for an Open Banking certificate for digital signature and authentication using OpenSSL. The following fields  must be changed by the client to specific data pertinent to them.

countryName = "GE"
localityName = "Tbilisi"
organizationName = "Open Banking"
commonName = openbanking.ge
organizationIdentifier = "PSDGE-NBG-CBASGE22"

subjectAltName = DNS.1:*.testexample1.ge, DNS.2:*.testexample2.ge


 As for the RootConfig.cnf it used to generate a root certificate CSR and OCSP certificate, no changes are required.


## Create and verify Root key and Certificate
>Create Root certificate need only one Time


### Generate root  key
>this is the key used to sign the certificate requests, anyone holding this can sign certificates on your behalf. So keep it in a safe place!

	openssl genrsa -des3 -out private/root.key 4096
	
**genrsa** - generate an RSA private key

**-des3**  - These options encrypt the private key with specified cipher before outputting it. If encryption is used a pass phrase is prompted for if it is not supplied via the **-passout** argument.

**-out [FileName]** - Output the key to the specified file. If this argument is not specified then standard output is used.

### Extract public key from root  key

	openssl rsa -in private/root.key -pubout -out root_public.key

**rsa** command processes RSA keys. They can be converted between various forms and their components printed out. **Note** this command uses the traditional SSLeay compatible format for private key encryption: newer applications should use the more secure PKCS#8 format using the **pkcs8** utility.

**-in filename** This specifies the input filename to read a key from or standard input if this option is not specified. If the key is encrypted a pass phrase will be prompted for.

**-pubout** by default a private key is output: with this option a public key will be output instead. This option is automatically set if the input is a public key.

**-out filename** This specifies the output filename to write a key to or standard output if this option is not specified. If any encryption options are set then a pass phrase will be prompted for. The output filename should not be the same as the input filename.

### Creat Certificate Signing Request (CSR)

	openssl req -new -key private/root.key -out root.csr -config conf/RootConfig.cnf

**req** command primarily creates and processes certificate requests in PKCS#10 format. It can additionally create self signed certificates for use as root CAs for example.

**-new** this option generates a new certificate request. It will prompt the user for the relevant field values. The actual fields prompted for and their maximum and minimum sizes are specified in the configuration file and any requested extensions.
If the  **-key**  option is not used it will generate a new RSA private key using information specified in the configuration file.

**-key filename** This specifies the file to read the private key from. It also accepts PKCS#8 format private keys for PEM format files.

**-out filename** This specifies the output filename to write to or standard output by default.

**-config filename** this allows an alternative configuration file to be specified, this overrides the compile time filename or any specified in the  **OPENSSL_CONF**  environment variable.


### Verify the CSR's content
	openssl req -in root.csr -noout -text


**req** command primarily creates and processes certificate requests in PKCS#10 format. It can additionally create self signed certificates for use as root CAs for example.

**-in filename** This specifies the input filename to read a request from or standard input if this option is not specified. A request is only read if the creation options (**-new**  and  **-newkey**) are not specified.

**-noout** this option prevents output of the encoded version of the request.

## Generate x 509 CA certificate

	openssl req -x509 -in root.csr -out root.crt  -key private/root.key  -nodes -sha256 -days 1024

**req** command primarily creates and processes certificate requests in PKCS#10 format. It can additionally create self signed certificates for use as root CAs for example.

**-x509** this option outputs a self signed certificate instead of a certificate request. This is typically used to generate a test certificate or a self signed root CA. The extensions added to the certificate (if any) are specified in the configuration file. Unless specified using the  **set_serial**  option, a large random number will be used for the serial number.
If existing request is specified with the  **-in**  option, it is converted to the self signed certificate otherwise new request is created.

**-in filename** This specifies the input filename to read a request from or standard input if this option is not specified. A request is only read if the creation options (**-new**  and  **-newkey**) are not specified.

**-out filename** This specifies the output filename to write to or standard output by default.

**-key filename** This specifies the file to read the private key from. It also accepts PKCS#8 format private keys for PEM format files.

**-days n** when the  **-x509**  option is being used this specifies the number of days to certify the certificate for. The default is 30 days.

### Verify the certificate's content
	openssl x509 -in root.crt -text -noout

**x509** command is a multi purpose certificate utility. It can be used to display certificate information, convert certificates to various forms, sign certificate requests like a "mini CA" or edit certificate trust settings.

**-in filename**  This specifies the input filename to read a certificate from or standard input if this option is not specified.

**-text** prints out the certificate in text form. Full details are output including the public key, signature algorithms, issuer and subject names, serial number any extensions present and any trust settings.

**-noout** this option prevents output of the encoded version of the request.

## Generate the X509 QWAC PSD2 certificate and sign with the  Root key
>This procedure needs to be followed for each server/appliance that needs a trusted certificate.

### Generate PSD2 QWAC Certificate key

	openssl genrsa -out private/psd2_QWAC_certificate.key 2048



**genrsa** - generate an RSA private key

**-des3**  - These options encrypt the private key with specified cipher before outputting it. If encryption is used a pass phrase is prompted for if it is not supplied via the **-passout** argument.

**-out [FileName]** - Output the key to the specified file. If this argument is not specified then standard output is used.



### Extract public key from root key

	openssl rsa -in private/psd2_QWAC_certificate.key -pubout -out psd2_QWAC_certificate_public.key

**rsa** command processes RSA keys. They can be converted between various forms and their components printed out. **Note** this command uses the traditional SSLeay compatible format for private key encryption: newer applications should use the more secure PKCS#8 format using the **pkcs8** utility.

**-in filename** This specifies the input filename to read a key from or standard input if this option is not specified. If the key is encrypted a pass phrase will be prompted for.

**-pubout** by default a private key is output: with this option a public key will be output instead. This option is automatically set if the input is a public key.

**-out filename** This specifies the output filename to write a key to or standard output if this option is not specified. If any encryption options are set then a pass phrase will be prompted for. The output filename should not be the same as the input filename.
	
### Creat QWAC Certificate Signing Request (CSR)

	openssl req -new -key private/psd2_QWAC_certificate.key -out psd2_QWAC_certificate.csr -config conf/PSD2_QWAC_config.cnf


**req** command primarily creates and processes certificate requests in PKCS#10 format. It can additionally create self signed certificates for use as root CAs for example.

**-new** this option generates a new certificate request. It will prompt the user for the relevant field values. The actual fields prompted for and their maximum and minimum sizes are specified in the configuration file and any requested extensions.
If the  **-key**  option is not used it will generate a new RSA private key using information specified in the configuration file.

**-key filename** This specifies the file to read the private key from. It also accepts PKCS#8 format private keys for PEM format files.

**-out filename** This specifies the output filename to write to or standard output by default.

**-config filename** this allows an alternative configuration file to be specified, this overrides the compile time filename or any specified in the  **OPENSSL_CONF**  environment variable.


### Verify the csr's content

	openssl req -in psd2_QWAC_certificate.csr -noout -text

**req** command primarily creates and processes certificate requests in PKCS#10 format. It can additionally create self signed certificates for use as root CAs for example.

**-in filename** This specifies the input filename to read a request from or standard input if this option is not specified. A request is only read if the creation options (**-new**  and  **-newkey**) are not specified.

**-noout** this option prevents output of the encoded version of the request.


### Generate the certificate using csr and key to sign with the CA Root key

If you are going to use certificate in CA sign certificate with root key and add it to CA database

    openssl ca -config conf/RootConfig.cnf -policy policy_anything -out psd2_QWAC_certificate.crt -infiles psd2_QWAC_certificate.csr  -extfile conf/PSD2_QWAC_config.cnf -extensions client_reqext

or sign certificate with root key only.

	openssl x509 -req -in psd2_QWAC_certificate.csr -CA root.crt -CAkey private/root.key -CAcreateserial -out psd2_QWAC_certificate.crt -extfile conf/PSD2_QWAC_config.cnf -extensions client_reqext -days 365 -sha256


**ca** command is a minimal CA application. It can be used to sign certificate requests in a variety of forms and generate CRLs it also maintains a text database of issued certificates and their status.

**-batch** this sets the batch mode. In this mode no questions will be asked and all certificates will be certified automatically.

**-config** filename specifies the configuration file to use.

**-policy** arg this option defines the CA "policy" to use. This is a section in the configuration file which decides which fields should be mandatory or match the CA certificate. Check out the **POLICY FORMAT** section for more information.

**POLICY FORMAT** The policy section consists of a set of variables corresponding to certificate DN fields. If the value is "match" then the field value must match the same field in the CA certificate. If the value is "supplied" then it must be present. If the value is "optional" then it may be present. Any fields not mentioned in the policy section are silently deleted, unless the -preserveDN option is set but this can be regarded more of a quirk than intended behaviour.

**-out** filename the output file to output certificates to. The default is standard output. The certificate details will also be printed out to this file in PEM format (except that -spkac outputs DER format).

**-infiles** if present this should be the last option, all subsequent arguments are assumed to the the names of files containing certificate requests.

**x509** command is a multi purpose certificate utility. It can be used to display certificate information, convert certificates to various forms, sign certificate requests like a "mini CA" or edit certificate trust settings.

**-req** by default a certificate is expected on input. With this option a certificate request is expected instead.

**-in filename** This specifies the input filename to read a certificate from or standard input if this option is not specified.

**-CA filename** specifies the CA certificate to be used for signing. When this option is present  **x509**  behaves like a "mini CA". The input file is signed by this CA using this option: that is its issuer name is set to the subject name of the CA and it is digitally signed using the CAs private key.
This option is normally combined with the  **-req**  option. Without the  **-req**  option the input is a certificate which must be self signed.

**-CAkey filename** sets the CA private key to sign a certificate with. If this option is not specified then it is assumed that the CA private key is present in the CA certificate file.

**-CAcreateserial** with this option the CA serial number file is created if it does not exist: it will contain the serial number "02" and the certificate being signed will have the 1 as its serial number. Normally if the  **-CA**  option is specified and the serial number file does not exist it is an error.

**-extfile filename** file containing certificate extensions to use. If not specified then no extensions are added to the certificate.

**-extensions section** the section to add certificate extensions from. If this option is not specified then the extensions should either be contained in the unnamed (default) section or the default section should contain a variable called "extensions" which contains the section to use. See the  [x509v3_config(5)](https://www.openssl.org/docs/man1.0.2/man5/x509v3_config.html)  manual page for details of the extension section format.

**-days arg** specifies the number of days to make a certificate valid for. The default is 30 days.

### Verify the certificate's content
	openssl x509 -in psd2_QWAC_certificate.crt -text -noout

**x509** command is a multi purpose certificate utility. It can be used to display certificate information, convert certificates to various forms, sign certificate requests like a "mini CA" or edit certificate trust settings.

**-in filename**  This specifies the input filename to read a certificate from or standard input if this option is not specified.

**-text** prints out the certificate in text form. Full details are output including the public key, signature algorithms, issuer and subject names, serial number any extensions present and any trust settings.

**-noout** this option prevents output of the encoded version of the request.

## Generate the X509 QSEAL PSD2 certificate and sign with the  Root key
>This procedure needs to be followed for each server/appliance that needs a trusted certificate from

### Generate PSD2 Certificate key

	openssl genrsa -out private/psd2_QSEAL_certificate.key 2048



**genrsa** - generate an RSA private key

**-des3**  - These options encrypt the private key with specified cipher before outputting it. If encryption is used a pass phrase is prompted for if it is not supplied via the **-passout** argument.

**-out [FileName]** - Output the key to the specified file. If this argument is not specified then standard output is used.



### Extract public key from root key

	openssl rsa -in private/psd2_QSEAL_certificate.key -pubout -out psd2_QSEAL_certificate_public.key

**rsa** command processes RSA keys. They can be converted between various forms and their components printed out. **Note** this command uses the traditional SSLeay compatible format for private key encryption: newer applications should use the more secure PKCS#8 format using the **pkcs8** utility.

**-in filename** This specifies the input filename to read a key from or standard input if this option is not specified. If the key is encrypted a pass phrase will be prompted for.

**-pubout** by default a private key is output: with this option a public key will be output instead. This option is automatically set if the input is a public key.

**-out filename** This specifies the output filename to write a key to or standard output if this option is not specified. If any encryption options are set then a pass phrase will be prompted for. The output filename should not be the same as the input filename.
	
### Creat Certificate Signing Request (CSR)

	openssl req -new -key private/psd2_QSEAL_certificate.key -out psd2_QSEAL_certificate.csr -config conf/PSD2_QSEAL_config.cnf


**req** command primarily creates and processes certificate requests in PKCS#10 format. It can additionally create self signed certificates for use as root CAs for example.

**-new** this option generates a new certificate request. It will prompt the user for the relevant field values. The actual fields prompted for and their maximum and minimum sizes are specified in the configuration file and any requested extensions.
If the  **-key**  option is not used it will generate a new RSA private key using information specified in the configuration file.

**-key filename** This specifies the file to read the private key from. It also accepts PKCS#8 format private keys for PEM format files.

**-out filename** This specifies the output filename to write to or standard output by default.

**-config filename** this allows an alternative configuration file to be specified, this overrides the compile time filename or any specified in the  **OPENSSL_CONF**  environment variable.


### Verify the csr's content

	openssl req -in psd2_QSEAL_certificate.csr -noout -text

**req** command primarily creates and processes certificate requests in PKCS#10 format. It can additionally create self signed certificates for use as root CAs for example.

**-in filename** This specifies the input filename to read a request from or standard input if this option is not specified. A request is only read if the creation options (**-new**  and  **-newkey**) are not specified.

**-noout** this option prevents output of the encoded version of the request.


### Generate the certificate using csr and key to sign with the CA Root key

If you are going to use certificate in CA sign certificate with root key and add it to CA database

    openssl ca -config conf/RootConfig.cnf -policy policy_anything -out psd2_QSEAL_certificate.crt -infiles psd2_QSEAL_certificate.csr  -extfile conf/PSD2_QSEAL_config.cnf -extensions client_reqext

or sign certificate with root key only.

	openssl x509 -req -in psd2_QSEAL_certificate.csr -CA root.crt -CAkey private/root.key -CAcreateserial -out psd2_QSEAL_certificate.crt -extfile conf/PSD2_QSEAL_config.cnf -extensions client_reqext -days 365 -sha256


**ca** command is a minimal CA application. It can be used to sign certificate requests in a variety of forms and generate CRLs it also maintains a text database of issued certificates and their status.

**-batch** this sets the batch mode. In this mode no questions will be asked and all certificates will be certified automatically.

**-config** filename specifies the configuration file to use.

**-policy** arg this option defines the CA "policy" to use. This is a section in the configuration file which decides which fields should be mandatory or match the CA certificate. Check out the **POLICY FORMAT** section for more information.

**POLICY FORMAT** The policy section consists of a set of variables corresponding to certificate DN fields. If the value is "match" then the field value must match the same field in the CA certificate. If the value is "supplied" then it must be present. If the value is "optional" then it may be present. Any fields not mentioned in the policy section are silently deleted, unless the -preserveDN option is set but this can be regarded more of a quirk than intended behaviour.

**-out** filename the output file to output certificates to. The default is standard output. The certificate details will also be printed out to this file in PEM format (except that -spkac outputs DER format).

**-infiles** if present this should be the last option, all subsequent arguments are assumed to the the names of files containing certificate requests.

**x509** command is a multi purpose certificate utility. It can be used to display certificate information, convert certificates to various forms, sign certificate requests like a "mini CA" or edit certificate trust settings.

**-req** by default a certificate is expected on input. With this option a certificate request is expected instead.

**-in filename** This specifies the input filename to read a certificate from or standard input if this option is not specified.

**-CA filename** specifies the CA certificate to be used for signing. When this option is present  **x509**  behaves like a "mini CA". The input file is signed by this CA using this option: that is its issuer name is set to the subject name of the CA and it is digitally signed using the CAs private key.
This option is normally combined with the  **-req**  option. Without the  **-req**  option the input is a certificate which must be self signed.

**-CAkey filename** sets the CA private key to sign a certificate with. If this option is not specified then it is assumed that the CA private key is present in the CA certificate file.

**-CAcreateserial** with this option the CA serial number file is created if it does not exist: it will contain the serial number "02" and the certificate being signed will have the 1 as its serial number. Normally if the  **-CA**  option is specified and the serial number file does not exist it is an error.

**-extfile filename** file containing certificate extensions to use. If not specified then no extensions are added to the certificate.

**-extensions section** the section to add certificate extensions from. If this option is not specified then the extensions should either be contained in the unnamed (default) section or the default section should contain a variable called "extensions" which contains the section to use. See the  [x509v3_config(5)](https://www.openssl.org/docs/man1.0.2/man5/x509v3_config.html)  manual page for details of the extension section format.

**-days arg** specifies the number of days to make a certificate valid for. The default is 30 days.

### Verify the certificate's content
	openssl x509 -in psd2_QSEAL_certificate.crt -text -noout

**x509** command is a multi purpose certificate utility. It can be used to display certificate information, convert certificates to various forms, sign certificate requests like a "mini CA" or edit certificate trust settings.

**-in filename**  This specifies the input filename to read a certificate from or standard input if this option is not specified.

**-text** prints out the certificate in text form. Full details are output including the public key, signature algorithms, issuer and subject names, serial number any extensions present and any trust settings.

**-noout** this option prevents output of the encoded version of the request.



## Create the OCSP server

To host an OCSP server it's necessary to create OCSP signing certificate

1. Create OCSP signing certificate key.
2. Create OCSP signing certificate CSR.
3. Create OCSP signing certificate.

### Create  OCSP signing certificate

>Create OCSP certificate need only one Time

Create OCSP Certificate key.

    openssl genrsa -des3 -out private/ocsp.key 4096

Create csr for OCSP certificate.

    openssl req -new -key private/ocsp.key -out ocsp.csr -config conf/RootConfig.cnf

Generate certificate sign with root key and add it to OCSP data base

    openssl ca -batch -config conf/RootConfig.cnf -policy policy_anything  -out ocsp.crt -infiles ocsp.csr

**ca** command is a minimal CA application. It can be used to sign certificate requests in a variety of forms and generate CRLs it also maintains a text database of issued certificates and their status.

**-batch** this sets the batch mode. In this mode no questions will be asked and all certificates will be certified automatically.

**-config** filename specifies the configuration file to use.

**-policy** arg this option defines the CA "policy" to use. This is a section in the configuration file which decides which fields should be mandatory or match the CA certificate. Check out the **POLICY FORMAT** section for more information.

**POLICY FORMAT** The policy section consists of a set of variables corresponding to certificate DN fields. If the value is "match" then the field value must match the same field in the CA certificate. If the value is "supplied" then it must be present. If the value is "optional" then it may be present. Any fields not mentioned in the policy section are silently deleted, unless the -preserveDN option is set but this can be regarded more of a quirk than intended behaviour.

**-out** filename the output file to output certificates to. The default is standard output. The certificate details will also be printed out to this file in PEM format (except that -spkac outputs DER format).

**-infiles** if present this should be the last option, all subsequent arguments are assumed to the the names of files containing certificate requests.

### Start OCSP Server. 

Switch to a new terminal and run

    openssl ocsp -CApath certs  -index index.txt -port 5000 -rkey private/ocsp.key -rsigner ocsp.crt -CA root.crt -text


**ocsp** command performs many common OCSP tasks. It can be used to print out requests and responses, create requests and send queries to an OCSP responder and behave like a mini OCSP server itself.

**-CAfile file, -CApath pathname** file or pathname containing trusted CA certificates. These are used to verify the signature on the OCSP response.

**-index indexfile** indexfile is a text index file in ca format containing certificate revocation information.

**-port** portnum Port to listen for OCSP requests on. The port may also be specified using the url option.

**-rkey** file The private key to sign OCSP responses with: if not present the file specified in the rsigner option is used.

**-rsigner** file The certificate to sign OCSP responses with.

**-CA** file CA certificate corresponding to the revocation information in indexfile.

**-text** print out the text form of the OCSP request, response or both respectively.



### Verify Certificate Revocation

Switch to a new terminal and run

    openssl ocsp -issuer root.crt -VAfile ocsp.crt -cert psd2certificate.crt -url http://127.0.01:5000

This will show that the certificate status is good.

**ocsp** command performs many common OCSP tasks. It can be used to print out requests and responses, create requests and send queries to an OCSP responder and behave like a mini OCSP server itself.

**-issuer** filename This specifies the current issuer certificate. This option can be used multiple times. The certificate specified in filename must be in PEM format. This option MUST come before any -cert options.

**-VAfile** file file containing explicitly trusted responder certificates. Equivalent to the -verify_other and -trust_other options.

**-cert** filename Add the certificate filename to the request. The issuer certificate is taken from the previous issuer option, or an error occurs if no issuer certificate is specified.

**-url** responder_url specify the responder URL. Both HTTP and HTTPS (SSL/TLS) URLs can be specified.



### Revoke a certificate

If you want to revoke the certificate run following command

    openssl ca -config conf/openssl.cnf  -keyfile root.key -cert root.crt -revoke psd2certificate.crt

Then restart the OCSP server.


Verify Certificate Revocation. Switch to a new terminal and run

    openssl ocsp -issuer root.crt -VAfile ocsp.crt -cert psd2certificate.crt -url http://127.0.01:5000

This will show that the certificate status as revoked.

**ca** command is a minimal CA application. It can be used to sign certificate requests in a variety of forms and generate CRLs it also maintains a text database of issued certificates and their status.

**-config** filename specifies the configuration file to use.

**-keyfile** filename the private key to sign requests with.

**-cert** the CA certificate file.

**-revoke filename** a filename containing a certificate to revoke.


### Publish your OCSP server to the internet using NGROCK

**NGROCK** is a cross-platform application that enables developers to expose a local OCSP server to the Internet with minimal effort. The software makes your locally-hosted OCSP server appear to be hosted on a subdomain of ngrok.com meaning that no public IP or domain name on the local machine is needed.

register ngrok.com web site and  download NGROCK from : https://dashboard.ngrok.com/get-started/setup 
After that you install NGROCK  and start application will appear command prompt.

You need to connect your account to your local NGROCK application running following command:

     ngrok authtoken [ token ]
     
**token** is provided NGROCK  "Setup & Installation"  page in the "Connect your account" section.

Once you have linked your account you will be given the opportunity to publish you OCSP server by running following command

    ngrok http 5000

you can see session status  in command prompt

# BIBLIOGRAPHY
Electronic Signatures and Infrastructures (ESI); Sector Specific Requirements; Qualified Certificate Profiles and TSP Policy Requirements under the payment services Directive (EU) 2015/2366”
https://www.etsi.org/deliver/etsi_ts/119400_119499/119495/01.02.01_60/ts_119495v010201p.pdf


